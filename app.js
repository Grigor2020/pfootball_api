var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var querys = require('./model/querys/querys');


var mids = require('./middleware')



var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var countriesRouter = require('./routes/football/countries');
var leaguesRouter = require('./routes/football/leagues');
var teamsRouter = require('./routes/football/teams');
var matchRouter = require('./routes/football/match');
var playerRouter = require('./routes/football/player');
var livescoresRouter = require('./routes/football/livescores');
var authRouter = require('./routes/auth/auth');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// app.use(function (req, res, next) {mids.getTimeInfo(req, res, next)});

app.use(function (req, res, next) {
  // var ip = req.connection.remoteAddress;
  // console.log('ip',ip)
  res.setHeader("Access-Control-Allow-Headers", "x-access-token,void,authorization,guest");
  //res.setHeader("Access-Control-Allow-Origin", "http://localhost:3000");
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Credentials", true);
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT'); // OPTIONS, PATCH,
  res.setHeader('Cache-Control', 'no-cache'); // OPTIONS, PATCH,
  res.setHeader('X-Frame-Options', 'deny'); // OPTIONS, PATCH,
  res.removeHeader('Server');
  res.removeHeader('X-Powered-By');
  next()
})


app.use(function (req, res, next) {
  if (req.headers['authorization'] == "efe7fc01f90652d1s53f168sd1513ew156dsc44115d33aebf") {
    next();
  } else {
    res.json({error: true, msg: "Authorization wrong", data: null});
    res.end();
  }
});

app.use(function (req, res, next) {

  if(req.method === "POST") {

      if (typeof req.body.timezone === "undefined") {
        req.timezone = "Asia/Yerevan";
      } else {
        req.timezone = req.body.timezone
      }

  }
  next();
});

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/auth', authRouter);
app.use('/football/countries', countriesRouter);
app.use('/football/leagues', leaguesRouter);
app.use('/football/teams', teamsRouter);
app.use('/football/player', playerRouter);
app.use('/football/match', matchRouter);
app.use('/football/livescores', livescoresRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
