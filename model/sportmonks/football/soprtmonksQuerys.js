const db = require('../../connection');
var log = require("../../../logs/log");
const statics = require('../../../static');
var SportmonksApi = require('sportmonks').SportmonksApi;
var sportmonks = new SportmonksApi("mHDiziH7d7ZRcVbkq3tF6yrPxgYDzjgfIsqHiSyOQoy0ZR5iWuDaQaz149vm");
var request = require('request');
var convertTime = require('../../../object/converttime')

var getFootballStandingsBySeasonId = function(seasonId)  {
    return new Promise(function (resolve, reject) {
        sportmonks.get(`v2.0/standings/season/${seasonId}`)
            .then(function (data) {
                resolve(data)
            })
            .catch(function (error) {
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"getFootballStandingsBySeasonId",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/soprtmonksQuerys.txt")
                reject(new Error("Sql error getFootballStandingsBySeasonId"))
            })
    })
}
var getFootballStandingsBySeasonIdNative = function(seasonId)  {
    return new Promise(function (resolve, reject) {
        var includes = "league,round,season"
        const options = {
            url: statics.SPORTMONKS_API_URL+"/standings/season/"+seasonId+"?api_token="+ statics.SPORTMONKS_API_TOKEN+"&include="+includes,
            method : "GET"
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    resolve(result.data)
                }else{
                    var errorData = {
                        code:error.code,
                        errno:error.errno,
                        sqlMessage:error.sqlMessage,
                        devData:{
                            queryFunction:"getFootballStandingsBySeasonIdNative",
                            date:new Date()
                        }
                    }
                    log.insertLog(JSON.stringify(errorData),"./logs/soprtmonksQuerys.txt")
                    reject(new Error("Sql error getFootballStandingsBySeasonId"))
                }
            }
        });
    })
}

var getFootballTopscorersBySeasonId = function(seasonId)  {
    return new Promise(function (resolve, reject) {
        var includes = "goalscorers.player,goalscorers.team,assistscorers.player,assistscorers.team"
        const options = {
            url: statics.SPORTMONKS_API_URL+"/topscorers/season/"+seasonId+"?api_token="+ statics.SPORTMONKS_API_TOKEN+"&include="+includes,
            method : "GET"
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    resolve(result.data)
                }else{
                    var errorData = {
                        code:error.code,
                        errno:error.errno,
                        sqlMessage:error.sqlMessage,
                        devData:{
                            queryFunction:"getFootballTopscorersBySeasonId",
                            date:new Date()
                        }
                    }
                    log.insertLog(JSON.stringify(errorData),"./logs/soprtmonksQuerys.txt")
                    reject(new Error("Sql error getFootballStandingsBySeasonId"))
                }
            }
        });
    })
}


var getPlayerById = function(playerId)  {
    return new Promise(function (resolve, reject) {
        var includes = "position,team,stats:order(league_id|asc),stats.team,stats.season,stats.league,trophies.seasons,transfers,transfers.team,sidelined"
        const options = {
            url: statics.SPORTMONKS_API_URL+"/players/"+playerId+"?api_token="+ statics.SPORTMONKS_API_TOKEN+"&include="+includes,
            method : "GET"
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    resolve(result.data)
                }else{
                    var errorData = {
                        code:error.code,
                        errno:error.errno,
                        sqlMessage:error.sqlMessage,
                        devData:{
                            queryFunction:"getFootballTopscorersBySeasonId",
                            date:new Date()
                        }
                    }
                    log.insertLog(JSON.stringify(errorData),"./logs/soprtmonksQuerys.txt")
                    reject(new Error("Sql error getPlayerById"))
                }
            }
        });
    })
}


var getMatchDetailsByMatchId = function(matchId,plusSeconds){
    return new Promise(function (resolve, reject) {
        var includes = "goals,localTeam,visitorTeam,league,season,round,stage,stats,events,referee,venue," +
            "localCoach,visitorCoach,lineup,lineup.player,lineup.player.country"
        const options = {
            url: statics.SPORTMONKS_API_URL+"/fixtures/"+matchId+"?api_token="+ statics.SPORTMONKS_API_TOKEN+"&include="+includes,
            method : "GET"
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    resolve(result.data)
                }else{
                    var errorData = {
                        code:error.code,
                        errno:error.errno,
                        sqlMessage:error.sqlMessage,
                        devData:{
                            queryFunction:"getMatchDetailsByMatchId",
                            date:new Date()
                        }
                    }
                    log.insertLog(JSON.stringify(errorData),"./logs/soprtmonksQuerys.txt")
                    reject(new Error("Sql error getFootballStandingsBySeasonId"))
                }
            }
        });
    })
}

var getMatchH2H = function(team1id,team2id,tz){
    return new Promise(function (resolve, reject) {
        var includes = "localTeam, visitorTeam, goals, league"
        const options = {
            url: statics.SPORTMONKS_API_URL+"/head2head/"+team1id+"/"+team2id+"?api_token="+ statics.SPORTMONKS_API_TOKEN+"&include="+includes+tz,
            method : "GET"
        };

        request(options, function (error, response, body) {
            console.log('response.statusCode ',error)
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);

                if(!result.error){
                    resolve(result.data)
                }else{
                    var errorData = {
                        code:error.code,
                        errno:error.errno,
                        sqlMessage:error.sqlMessage,
                        devData:{
                            queryFunction:"getMatchDetailsByMatchId",
                            date:new Date()
                        }
                    }
                    log.insertLog(JSON.stringify(errorData),"./logs/soprtmonksQuerys.txt")
                    reject(new Error("Sql error getMatchH2H"))
                }
            }else{
                log.insertLog('soprtmonksQuerys.getMatchH2H() -> '+response.statusCode,"./logs/soprtmonksQuerys.txt")
                resolve([])
            }
        });
    })
}

var getFootballResultsBySeasonId = function(seasonId){
    return new Promise(function (resolve, reject) {
        var includes = "cards,goals,localTeam,visitorTeam,substitutions,league,season,round,stage,stats,events"
        const options = {
            url: statics.SPORTMONKS_API_URL+"/fixtures/"+seasonId+"?api_token="+ statics.SPORTMONKS_API_TOKEN+"&include="+includes,
            method : "GET"
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    resolve(result.data)
                }else{
                    var errorData = {
                        code:error.code,
                        errno:error.errno,
                        sqlMessage:error.sqlMessage,
                        devData:{
                            queryFunction:"getFootballResultsBySeasonId",
                            date:new Date()
                        }
                    }
                    log.insertLog(JSON.stringify(errorData),"./logs/soprtmonksQuerys.txt")
                    reject(new Error("Sql error getFootballStandingsBySeasonId"))
                }
            }
        });
    })
}

var getFootballLeagueByLeagueId = function(league_id){
    return new Promise(function (resolve, reject) {
        var includes = "country,season "
        const options = {
            url: statics.SPORTMONKS_API_URL+"/leagues/"+league_id+"?api_token="+ statics.SPORTMONKS_API_TOKEN+"&include="+includes,
            method : "GET"
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    resolve(result.data)
                }else{
                    var errorData = {
                        code:error.code,
                        errno:error.errno,
                        sqlMessage:error.sqlMessage,
                        devData:{
                            queryFunction:"getFootballLeagueByLeagueId",
                            date:new Date()
                        }
                    }
                    log.insertLog(JSON.stringify(errorData),"./logs/soprtmonksQuerys.txt")
                    reject(new Error("Sql error getFootballStandingsBySeasonId"))
                }
            }
        });
    })
}

var getFootballRoundBySeasonID = function(leagueId,season_id,tz){
    return new Promise(function (resolve, reject) {
        // var includes = " stats"
        // const options = {
        //     url: statics.SPORTMONKS_API_URL+"/leagues/"+leagueId+"?api_token="+ statics.SPORTMONKS_API_TOKEN+"&include="+includes+tz,
        //     method : "GET"
        // };
        var includes = " fixtures.localTeam, fixtures.visitorTeam,league, results.localTeam, results.visitorTeam"
        const options = {
            url: statics.SPORTMONKS_API_URL+"/rounds/season/"+season_id+"?api_token="+ statics.SPORTMONKS_API_TOKEN+"&include="+includes+tz,
            method : "GET"
        };
        //console.log('options',options)
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    resolve(result.data)
                }else{
                    var errorData = {
                        code:error.code,
                        errno:error.errno,
                        sqlMessage:error.sqlMessage,
                        devData:{
                            queryFunction:"getFootballRoundBySeasonID",
                            date:new Date()
                        }
                    }
                    log.insertLog(JSON.stringify(errorData),"./logs/soprtmonksQuerys.txt")
                    reject(new Error("Sql error getFootballStandingsBySeasonId"))
                }
            }
        });
    })
}

var getFootballTeamById = function(team_id){
    return new Promise(function (resolve, reject) {
        var includes = "country"
        const options = {
            url: statics.SPORTMONKS_API_URL+"/teams/"+team_id+"?api_token="+ statics.SPORTMONKS_API_TOKEN+"&include="+includes,
            method : "GET"
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    resolve(result.data)
                }else{
                    var errorData = {
                        code:error.code,
                        errno:error.errno,
                        sqlMessage:error.sqlMessage,
                        devData:{
                            queryFunction:"getFootballTeamById",
                            date:new Date()
                        }
                    }
                    log.insertLog(JSON.stringify(errorData),"./logs/soprtmonksQuerys.txt")
                    reject(new Error("Sql error getFootballStandingsBySeasonId"))
                }
            }
        });
    })
}

var getFootballTeamByIdWithOptions = function(team_id,tz){
    return new Promise(function (resolve, reject) {
        var includes = "" +
            "squad.player,squad.position," +
            "coach,transfers:order(date|desc)," +
            "transfers.player,transfers.team," +
            "latest.visitorTeam,latest.localTeam,latest.league," +
            "upcoming.visitorTeam,upcoming.localTeam,upcoming.league"
        const options = {
            url: statics.SPORTMONKS_API_URL+"/teams/"+team_id+"?api_token="+ statics.SPORTMONKS_API_TOKEN+"&include="+includes+tz,
            method : "GET"
        };
        //console.log('options',options)
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    resolve(result.data)
                }else{
                    var errorData = {
                        code:error.code,
                        errno:error.errno,
                        sqlMessage:error.sqlMessage,
                        devData:{
                            queryFunction:"getFootballTeamByIdWithOptions",
                            date:new Date()
                        }
                    }
                    log.insertLog(JSON.stringify(errorData),"./logs/soprtmonksQuerys.txt")
                    reject(new Error("Sql error getFootballStandingsBySeasonId"))
                }
            }
        });
    })
}

var getFootballTeamFixtures = function(team_id){
    return new Promise(function (resolve, reject) {
        var dates = findDatesBetweenTodayAndTwoWeeks();
        //console.log('v',dates)
        var includes = "localTeam,visitorTeam,round"
        const options = {
            url: statics.SPORTMONKS_API_URL+"/teams/between/"+dates.today+"/"+dates.dayTwoWeeks+"/"+team_id+"?api_token="+ statics.SPORTMONKS_API_TOKEN+"&include="+includes,
            method : "GET"
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    resolve(result.data)
                }else{
                    var errorData = {
                        code:error.code,
                        errno:error.errno,
                        sqlMessage:error.sqlMessage,
                        devData:{
                            queryFunction:"getFootballTeamFixtures",
                            date:new Date()
                        }
                    }
                    log.insertLog(JSON.stringify(errorData),"./logs/soprtmonksQuerys.txt")
                    reject(new Error("Sql error getFootballTeamFixtures"))
                }
            }
        });
    })
}

var getFootballCountry = function(countryId,tz){
    return new Promise(function (resolve, reject) {
        var includes = ""
        const options = {
            url: statics.SPORTMONKS_API_URL+"/leagues?api_token="+ statics.SPORTMONKS_API_TOKEN+"&countries="+countryId+"&include="+includes+tz,
            method : "GET"
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    // console.log('..............',result.data.leagues)
                    resolve(result.data)
                }else{
                    var errorData = {
                        code:error.code,
                        errno:error.errno,
                        sqlMessage:error.sqlMessage,
                        devData:{
                            queryFunction:"getFootballTeamFixtures",
                            date:new Date()
                        }
                    }
                    log.insertLog(JSON.stringify(errorData),"./logs/soprtmonksQuerys.txt")
                    reject(new Error("Sql error getFootballCountry"))
                }
            }
        });
    })
}

function findDatesBetweenTodayAndTwoWeeks(){
    var fortnightAway = new Date(Date.now() + 12096e5);
    var today = new Date();
    var ddF = String(today.getDate()).padStart(2, '0');
    var mmF = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyyF = today.getFullYear();
    var ddS = String(fortnightAway.getDate()).padStart(2, '0');
    var mmS = String(fortnightAway.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyyS = fortnightAway.getFullYear();

    var todays = yyyyF + '-' + mmF + '-' + ddF;
    var dayTwoWeeks = yyyyS + '-' + mmS + '-' + ddS;
    return {today : todays,dayTwoWeeks : dayTwoWeeks}
}


module.exports.getFootballCountry = getFootballCountry;
module.exports.getMatchH2H = getMatchH2H;
module.exports.getFootballTeamFixtures = getFootballTeamFixtures;
module.exports.getPlayerById = getPlayerById;
module.exports.getFootballTopscorersBySeasonId = getFootballTopscorersBySeasonId;
module.exports.getFootballStandingsBySeasonIdNative = getFootballStandingsBySeasonIdNative;
module.exports.getFootballStandingsBySeasonId = getFootballStandingsBySeasonId;
module.exports.getMatchDetailsByMatchId = getMatchDetailsByMatchId;
module.exports.getFootballResultsBySeasonId = getFootballResultsBySeasonId;
module.exports.getFootballLeagueByLeagueId = getFootballLeagueByLeagueId;
module.exports.getFootballRoundBySeasonID = getFootballRoundBySeasonID;
module.exports.getFootballTeamById = getFootballTeamById;
module.exports.getFootballTeamByIdWithOptions = getFootballTeamByIdWithOptions;

