const db = require('../connection');
var log = require("../../logs/log");
const static = require('../../static');

var findAllByFilds = function(table,filds,cb){
    var queryFild = "";
    if(filds == "*"){
        queryFild = filds;
    }else{
        queryFild = filds.join(' , ')
    }
    var prepareSql = 'SELECT ' + queryFild + ' FROM '+table+'';
    db.query(prepareSql, function (error, result, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage,
                devData:{
                    queryFunction:"findAllByFilds",
                    table:table,
                    filds:filds,
                    date:new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,result)
        }
    })
}



/**
 *
 * @param table
 * @param params {fild_name:value}
 * @param filds [fild1,fild2]
 * @param cb function
 */
var findByMultyName = function (table,params,filds,cb) {
    var whereParams = [];
    for(var key in params){
        if(params[key] !== "NULL") {
            whereParams.push("`" + key + "`=" + db.escape(params[key]));
        }else{
            whereParams.push("`" + key + "` IS NULL");
        }
    }
    var prepareSql = 'SELECT ' + filds.join(' , ') + ' FROM ' + table + ' WHERE '+whereParams.join(' AND ')+'';

    db.query(prepareSql, function (error, rows, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage,
                devData:{
                    queryFunction:"findByMultyName2v",
                    table:table,
                    params:params,
                    filds:filds,
                    date:new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,rows)
        }
    });
}

/**
 *
 * @param table
 * @param post {fildname:value}
 * @param cb function(){}
 */
var insert2v = function(table,post,cb){
    var prepareSql = 'INSERT INTO '+table+' SET ?';
    var query = db.query(prepareSql, post, function (error, results, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}

var findAllByFildsPromise = function(table,filds){
    return new Promise(function (resolve, reject) {
        var queryFild = "";
        if(filds == "*"){
            queryFild = filds;
        }else{
            queryFild = filds.join(' , ')
        }
        var prepareSql = 'SELECT ' + queryFild + ' FROM '+table+'';
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllByFilds",
                        table:table,
                        filds:filds,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error findAllByFildsPromise: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}
var findAllByFildsOrderedPromise = function(table,filds,order){
    return new Promise(function (resolve, reject) {
        var queryFild = "";
        if(filds == "*"){
            queryFild = filds;
        }else{
            queryFild = filds.join(' , ')
        }
        var orderString = '';
        if(order !== null){
            orderString = ' ORDER BY '+order;
        }
        var prepareSql = 'SELECT ' + queryFild + ' FROM '+table+''+orderString;
        // console.log(prepareSql)
        db.query(prepareSql, function (error, result, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findAllByFilds",
                        table:table,
                        filds:filds,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error findAllByFildsPromise: "+error.sqlMessage+""))
            }else{
                resolve(result)
            }
        })
    })
}

var findByMultyNamePromise = function(table,params,filds){
    return new Promise(function (resolve, reject) {
        var whereParams = [];
        for(var key in params){
            if(params[key] !== "NULL") {
                whereParams.push("`" + key + "`=" + db.escape(params[key]));
            }else{
                whereParams.push("`" + key + "` IS NULL");
            }
        }
        var prepareSql = 'SELECT ' + filds.join(' , ') + ' FROM ' + table + ' WHERE '+whereParams.join(' AND ')+'';
        // console.log('prepareSql',prepareSql)
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByMultyNamePromise",
                        table:table,
                        params:params,
                        filds:filds,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error findByMultyNamePromise"))
            }else{
                resolve(rows)
            }
        });
    })
}


function insert2vPromise(table,post){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'INSERT INTO '+table+' SET ?';
        //console.log('prepareSql',prepareSql)
        var query = db.query(prepareSql, post, function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"insert2vPromise",
                        table:table,
                        params:post,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error insert2vPromise"))
            }else{
                resolve(results)
            }
        });
    })
}

var insertLoopPromise = function(table,filds,post){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'INSERT INTO ' + table + ' (' + filds.join(',') + ') VALUES  ?';
        var query = db.query(prepareSql, [post], function (error, results, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"insertLoopPromise",
                        table:table,
                        filds:filds,
                        params:post,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error insertLoopPromise"))
            }else{
                resolve(results)
            }
        });
    })
}

function deletePromise(table,params) {
    return new Promise(function (resolve, reject) {
        //console.log('params',params)
        var where = [];
        for(var key in params){
            where.push("`" + key + "`=" + db.escape(params[key]));
        }

        // var whereParams = [];
        // for(var key in params){
        //     if(params[key] !== "NULL") {
        //         whereParams.push("`" + key + "`=" + db.escape(params[key]));
        //     }else{
        //         whereParams.push("`" + key + "` IS NULL");
        //     }
        // }
        //


        //console.log('where',where)
        var prepareSql = "DELETE FROM  " + table + " WHERE " + where.join(" AND ") + " ";
        // console.log('prepareSql',prepareSql)
        var query = db.query(prepareSql, function (error, results, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "deletePromise",
                        table: table,
                        params: params,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt");
                reject(new Error("Sql error deletePromise"))
            } else {
                resolve({error: false})
            }
        });
    })
}


function findByToken(token){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `id`, `email`,`token` FROM  `users_db` WHERE `token`=' + db.escape(token) + ' LIMIT 1';
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByToken",
                        table:'users_db',
                        params:{token:token},
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}


function findUserLikes(userId,type){
    return new Promise(function (resolve, reject) {
        var selectRow = '';
        var joinRow = '';
        if(type === '0'){
            selectRow = '`s_leagues`.`name`,`s_leagues`.`logo_path` ';
            joinRow = 'JOIN `s_leagues` ON `s_leagues`.`sportmonks_id` = `users_likes`.`sportmonks_id`';
        }
        if(type === '1'){
            selectRow = '`s_teams`.`name`,`s_teams`.`logo_path` ';
            joinRow = 'JOIN `s_teams` ON `s_teams`.`sportmonks_id` = `users_likes`.`sportmonks_id`';
        }
        var prepareSql = '' +
            'SELECT  `users_likes`.`sportmonks_id`,' +
            selectRow +
            'FROM  `users_likes` ' +
            joinRow +
            'WHERE `users_likes`.`user_id`=' + db.escape(userId)+' AND `users_likes`.`type` = '+db.escape(type);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findUserLikes",
                        table:'users_likes',
                        params:{userId:userId},
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}


function findByGuestToken(token){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `id` FROM  `guest` WHERE `token`=' + db.escape(token) + ' LIMIT 1';
        db.query(prepareSql, function (error, rows, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "findByGuestToken",
                        table: 'guest',
                        params: {token:token},
                        filds: "*",
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v2/checkUser.txt")
                reject(new Error("Sql error findByGuestToken"))
            } else {
                resolve(rows)
            }
        })
    })
}

var update2vPromise = function(table,post){
    return new Promise(function (resolve, reject) {
        var val = [];
        for (var key in post.values) {
            val.push("`" + key + "`=" + db.escape(post.values[key]));
        }
        var whereVal = [];
        for (var key in post.where) {
            whereVal.push("`" + key + "`" + "=" + db.escape(post.where[key]));
        }
        var prepareSql = "UPDATE " + table + " SET " + val.toString() + " WHERE " + whereVal.join(' AND ') + "";

        var query = db.query(prepareSql, post, function (error, results, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "update2vPromise",
                        table: table,
                        params: post,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt")
                resolve(results)
            } else {
                resolve(results)
            }
        });
    })
}

function updatePromise(table,post){
    return new Promise(function (resolve, reject) {
        var val = [];
        for(var key in post.values){
            val.push("`"+key +"`="+db.escape(post.values[key]));
        }
        var whereVal = [];
        for(var key in post.where){
            whereVal.push("`"+key+"`" +"="+db.escape(post.where[key]));
        }
        var prepareSql = "UPDATE "+ table + " SET "+val.toString()+" WHERE "+whereVal.join(' AND ')+"";
        var query = db.query(prepareSql, post, function (error, results, fields) {
            if (error){
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "updatePromise",
                        table: table,
                        params: post,
                        date: new Date()
                    }
                }

                if(errorData.errno == '1062' && (errorData.devData.table == 'orders' || errorData.devData.table == 'compare' || errorData.devData.table == 'favorite' || errorData.devData.table == 'product_last_show')){
                    getAndDeleteDuplicateKeysFromOrder(errorData.devData.table,errorData.devData.params.values.user_id,errorData.devData.params.where.guest_id,function (errorInfo) {
                        if(!errorInfo.error){
                            updatePromise("orders",errorData.devData.params)
                                .then(function (results) {
                                    resolve(results)
                                })
                                .catch(function () {
                                    log.insertLog(JSON.stringify({funcName:"getAndDeleteDuplicateKeysFromOrder"}), "./logs/query2v.txt");
                                    reject(new Error("Sql error updatePromise 2"));
                                })
                        }else{
                            deleteMultyRowBy('orders',[{key:"guest_id",value:errorData.devData.params.where.guest_id}],function (errorInfo) {
                                resolve(results)
                            })
                        }
                    })
                }else {
                    log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt");
                    reject(new Error("Sql error updatePromise"));
                }
            }else{
                resolve(results)
            }
        });
    })
}

function getAndDeleteDuplicateKeysFromOrder(table,user_id,guest_id,cb) {
    var prepareSql = "SELECT `"+table+"`.`id`,`orders` .`product_item_id`,COUNT(`"+table+"` .`product_item_id`) as cc FROM `"+table+"` "+
        "WHERE `"+table+"`.`user_id` = "+db.escape(user_id)+" OR `orders`.`guest_id` = "+db.escape(guest_id)+" ";
    var query = db.query(prepareSql, function (error, results, fields) {
        if (error){
            var errorData = {
                code: error.code,
                errno: error.errno,
                sqlMessage: error.sqlMessage,
                devData: {
                    queryFunction: "getEndDeleteDuplicateKeysFromOrder",
                    table: table,
                    params: {user_id:user_id,guest_id:guest_id},
                    date: new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt");
            cb({error:true})
        }else{
            var orderId = [];
            if(results.length>0) {
                for (var i = 0; i < results.length; i++) {
                    if (results[i]['cc'] > 1) {
                        orderId.push(
                            {
                                key:"id",
                                value:results[i]['id']
                            }
                        )
                    }
                }

                deleteMultyRowBy(table,orderId,function (errorInfo) {
                    cb(errorInfo)
                })
            }else{
                cb({error:true})
            }
        }
    })
}

function deleteMultyRowBy(table,params,cb) {
    var where = [];
    for(var i=0;i<params.length;i++){
        where.push("`"+params[i]['key'] +"`="+db.escape(params[i]['value']));
    }
    var prepareSql = "DELETE FROM  "+table+" WHERE "+where.join(" OR ")+" ";
    var query = db.query(prepareSql, function (error, results, fields) {
        if (error){
            var errorData = {
                code: error.code,
                errno: error.errno,
                sqlMessage: error.sqlMessage,
                devData: {
                    queryFunction: "deleteMultyRowBy",
                    table: table,
                    params: params,
                    date: new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt");
            cb({error:true})
        }else{
            cb({error:false})
        }
    });
}

var update2v = function(table,post,cb){
    var val = [];
    for(var key in post.values){
        val.push("`"+key +"`="+db.escape(post.values[key]));
    }
    var whereVal = [];
    for(var key in post.where){
        whereVal.push("`"+key+"`" +"="+db.escape(post.where[key]));
    }
    var prepareSql = "UPDATE "+ table + " SET "+val.toString()+" WHERE "+whereVal.join(' AND ')+"";
    var query = db.query(prepareSql, post, function (error, results, fields) {
        if (error){
            var errorData = {
                code: error.code,
                errno: error.errno,
                sqlMessage: error.sqlMessage,
                devData: {
                    queryFunction: "update2v",
                    table: table,
                    params: post,
                    date: new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}

/**
 *
 * @param table
 * @param post {fildname:value}
 * @param cb function(){}
 */
var deletes = function(table,post,cb){
    var val = [];
    for(var key in post){
        if(post[key] !== "NULL") {
            val.push("`" + key + "`=" + db.escape(post[key]));
        }else{
            val.push("`" + key + "` IS NULL");
        }
    }
    var prepareSql = 'DELETE FROM ' + table + ' WHERE '+val.join(' AND ')+'';
    db.query(prepareSql,post,function(error,result,fields){
        if (error){
            var errorData = {
                code: error.code,
                errno: error.errno,
                sqlMessage: error.sqlMessage,
                devData: {
                    queryFunction: "deletes",
                    table: table,
                    params: post,
                    date: new Date()
                }
            }
            log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
            cb(true,errorData);
        }else{
            cb(false,result)
        }
    });
}

var deletesPromise = function(table,post){
    return new Promise(function (resolve, reject) {
        var val = [];
        for (var key in post) {
            if (post[key] !== "NULL") {
                val.push("`" + key + "`=" + db.escape(post[key]));
            } else {
                val.push("`" + key + "` IS NULL");
            }
        }
        var prepareSql = 'DELETE FROM ' + table + ' WHERE ' + val.join(' AND ') + '';

        db.query(prepareSql, post, function (error, result, fields) {
            if (error) {
                var errorData = {
                    code: error.code,
                    errno: error.errno,
                    sqlMessage: error.sqlMessage,
                    devData: {
                        queryFunction: "deletesPromise",
                        table: table,
                        params: post,
                        date: new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData), "./logs/query2v.txt")
                resolve(errorData);
            } else {
                resolve(result)
            }
        });
    })
}


function findByEmail(email){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `id` FROM  `users_db` WHERE `email`=' + db.escape(email);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByEmail",
                        table:'users_db',
                        params:{token:token},
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v2/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}

function findByEmailAndCodeForForgotPassword(queryParams){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `id` FROM  `forgot_password_key` WHERE `user_id`=' + db.escape(queryParams.user_id)+' AND `ver_code`='+db.escape(queryParams.code);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByEmailAndCodeForForgotPassword",
                        table:'forgot_password_key',
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v2/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}
function findByCodeId(codeId){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `user_id` FROM  `forgot_password_key` WHERE `id`=' + db.escape(codeId);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByCodeId",
                        table:'forgot_password_key',
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v2/checkUser.txt")
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}





function findByUser(userid){
    return new Promise(function (resolve, reject) {
        var prepareSql = 'SELECT `id`,`email`,`f_name`,`l_name`,`m_name` FROM  `users_db` WHERE `id`=' + db.escape(userid);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"findByUser",
                        table:'users_db',
                        params:{userid:userid},
                        filds:"*",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/v2/findUser.txt");
                reject(new Error("Sql error "+error.sqlMessage+""))
            }else{
                resolve(rows)
            }
        })
    })
}

module.exports.findByUser = findByUser;


module.exports.findByEmail = findByEmail;
module.exports.findByEmailAndCodeForForgotPassword = findByEmailAndCodeForForgotPassword;
module.exports.findByCodeId = findByCodeId;

module.exports.findAllByFilds = findAllByFilds;
module.exports.findAllByFildsPromise = findAllByFildsPromise;
module.exports.findAllByFildsOrderedPromise = findAllByFildsOrderedPromise;
module.exports.findByMultyName = findByMultyName;
module.exports.findByMultyNamePromise = findByMultyNamePromise;
module.exports.insert2v = insert2v;
module.exports.update2v = update2v;
module.exports.updatePromise = updatePromise;
module.exports.update2vPromise = update2vPromise;
module.exports.deletes = deletes;
module.exports.deletesPromise = deletesPromise;
module.exports.insert2vPromise = insert2vPromise;
module.exports.insertLoopPromise = insertLoopPromise;
module.exports.deletePromise = deletePromise;
module.exports.findByToken = findByToken;
module.exports.findUserLikes = findUserLikes;
module.exports.findByGuestToken = findByGuestToken;
