const db = require('../connection');
var log = require("../../logs/log");
const static = require('../../static');


var checkDefineLeague = function(params,filds){
    return new Promise(function (resolve, reject) {
        var whereParams = [];
        for(var key in params){
            if(params[key] !== "NULL") {
                whereParams.push("`" + key + "`=" + db.escape(params[key]));
            }else{
                whereParams.push("`" + key + "` IS NULL");
            }
        }
        var prepareSql = 'SELECT ' + filds.join(' , ') + ' FROM `s_leagues` WHERE '+whereParams.join(' AND ')+'';
        // console.log('prepareSql',prepareSql)

        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    sqlMessage:error.sqlMessage,
                    devData:{
                        queryFunction:"checkDefineLeague",
                        table:'s_leagues',
                        params:params,
                        filds:filds,
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
                reject(new Error("Sql error findByMultyNamePromise"))
            }else{
                resolve(rows)
            }
        });
    })
}
// var findFirstPageLeagues = function(){
//     return new Promise(function (resolve, reject) {
//         var prepareSql = '' +
//             'SELECT `s_countries`.`sportmonks_id` as sportmonks_country_id, `s_countries`.`name` as country_name,' +
//             'GROUP_CONCAT(DISTINCT `s_leagues`.`name` ORDER BY `s_leagues`.`name`  SEPARATOR "||") AS leagues_name '+
//             'FROM `s_countries`' +
//             'JOIN `s_leagues` ON `s_leagues`.`s_country_id` = `s_countries`.`sportmonks_id`' +
//             'GROUP BY `s_countries`.`sportmonks_id` ORDER BY `s_countries`.`name`';
//         // console.log(prepareSql)
//         db.query(prepareSql, function (error, result, fields) {
//             if (error){
//                 var errorData = {
//                     code:error.code,
//                     errno:error.errno,
//                     sqlMessage:error.sqlMessage,
//                     devData:{
//                         queryFunction:"findAllByFilds",
//                         date:new Date()
//                     }
//                 }
//                 log.insertLog(JSON.stringify(errorData),"./logs/query2v.txt")
//                 reject(new Error("Sql error findAllByFildsPromise: "+error.sqlMessage+""))
//             }else{
//                 resolve(result)
//             }
//         })
//     })
// }

module.exports.checkDefineLeague = checkDefineLeague;
// module.exports.findFirstPageLeagues = findFirstPageLeagues;
