var leaguesQuery = require('../../model/querys/leagues');
var countryQuery = require('../../model/querys/country');
var querys = require('../../model/querys/querys');
var SportmonksApi = require('sportmonks').SportmonksApi;
var sportmonks = new SportmonksApi("mHDiziH7d7ZRcVbkq3tF6yrPxgYDzjgfIsqHiSyOQoy0ZR5iWuDaQaz149vm");

function GetTeams(teamId) {

    this.teamId = teamId;

    this.result = {
        // countryName:"",
        teamInfo: {}
    };

    var self = this;

    this.selectTeam = function () {
        return new Promise(function (resolve, reject) {
            // console.log('1111111',self.teamId)
            querys.findByMultyNamePromise('s_teams',{sportmonks_id:self.teamId},['*'])
                .then(function (data) {
                    // console.log("AAAAAAAAA",data)
                    if(data.length === 0){
                        // console.log('xxx')
                        self.getTeamFromApi()
                            .then(function (asd) {
                                self.result.teamInfo = asd;
                                resolve(self.result)
                            })
                            .catch(function (error) {
                                resolve({error:'657543445547',errMsg:error.toString()})
                            })
                    }else{
                        // console.log('yyy')
                        self.result.teamInfo = data[0];
                        resolve(self.result)
                    }
                })
                .catch(function (error) {
                    resolve({error:'8926598',errMsg:error.toString()})
                })
        })
    };


    this.getTeamFromApi = function () {
        return new Promise(function (resolve, reject) {
            // console.log('zzz')
            sportmonks.get(`v2.0/teams/${self.teamId}`)
                .then(function (data) {
                    var insertData = {
                        sportmonks_id :data.data.id,
                        name:data.data.name,
                        short_code:data.data.short_code,
                        s_country_id:data.data.country_id,
                        founded:data.data.founded,
                        logo_path:data.data.logo_path,
                        s_venue_id:data.data.s_venue_id
                    }
                    self.insertTeam(insertData)
                        .then(function (result) {
                            insertData.local_id = result.insertId;
                            // console.log('XSSSSSSSXXXXSSSXSXSXS');
                            resolve(insertData)
                        })
                        .catch(function (error) {
                            resolve({error:'6875345457',errMsg:error.toString()})
                        })
                })
                .catch(function (error) {
                    resolve({error:'534566577876',errMsg:error.toString()})
                })
        });
    };

    this.insertTeam = function (insertData) {
        return new Promise(function (resolve, reject) {
            querys.insert2vPromise("s_teams",insertData)
                .then(function (result) {
                    resolve(result)
                })
                .catch(function (error) {
                    resolve({error:true,errMsg:error.toString()})
                })
        })
    }
}


module.exports = GetTeams;
