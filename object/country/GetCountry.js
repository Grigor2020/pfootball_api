var leaguesQuery = require('../../model/querys/leagues');
var countryQuery = require('../../model/querys/country');
var querys = require('../../model/querys/querys');
var SportmonksApi = require('sportmonks').SportmonksApi;
var sportmonks = new SportmonksApi("mHDiziH7d7ZRcVbkq3tF6yrPxgYDzjgfIsqHiSyOQoy0ZR5iWuDaQaz149vm");

function GetCountry(countryId) {

    this.countryId = countryId;

    this.result = {
        // countryName:"",
        countryInfo: {}
    };

    var self = this;

    this.selectCountry = function () {
        return new Promise(function (resolve, reject) {
            querys.findByMultyNamePromise('s_countries',{sportmonks_id:self.countryId},['*'])
                .then(function (data) {
                    //console.log("AAAAAAAAA",data)
                    if(data.length === 0){
                        // console.log('xxx')
                        self.getCountryFromApi()
                            .then(function (asd) {
                                self.result.countryInfo = asd;
                                resolve(self.result)
                            })
                            .catch(function (error) {
                                resolve({error:'7896415',errMsg:error.toString()})
                            })
                    }else{
                        // console.log('yyy')
                        self.result.countryInfo = data[0];
                        resolve(self.result)
                    }
                })
                .catch(function (error) {
                    resolve({error:'8926598',errMsg:error.toString()})
                })
        })
    };


    this.getCountryFromApi = function () {
        return new Promise(function (resolve, reject) {
            //console.log('zzz',self.countryId)
            sportmonks.get(`v2.0/countries/${self.countryId}`)
                .then(function (data) {
                    //console.log('data',data.data)
                    var insertData = {
                        sportmonks_id :data.data.id,
                        name:data.data.name,
                        image_path:!data.data.image_path ? null : data.data.image_path,
                        continent: !data.data.extra ? null : data.data.extra.continent,
                        fifa:!data.data.extra ? null : data.data.extra.fifa,
                        longitude:!data.data.extra ? null : data.data.extra.longitude,
                        latitude:!data.data.extra ? null : data.data.extra.latitude,
                        flag:!data.data.extra ? null : data.data.extra.flag,
                    }
                    //console.log('insertData',insertData)
                    self.insertCountry(insertData)
                        .then(function (result) {
                            insertData.local_id = result.insertId;
                            resolve(insertData)
                        })
                        .catch(function (error) {
                            resolve({error:'864536864354',errMsg:error.toString()})
                        })
                })
                .catch(function (error) {
                    resolve({error:'6576634543546554',errMsg:error.toString()})
                })
        });
    };

    this.insertCountry = function (insertData) {
        return new Promise(function (resolve, reject) {
            querys.insert2vPromise("s_countries",insertData)
                .then(function (result) {
                    resolve(result)
                })
                .catch(function (error) {
                    resolve({error:true,errMsg:error.toString()})
                })
        })
    }
}


module.exports = GetCountry;
