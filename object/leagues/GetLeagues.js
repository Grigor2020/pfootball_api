var leaguesQuery = require('../../model/querys/leagues');
var SportmonksApi = require('sportmonks').SportmonksApi;
var sportmonks = new SportmonksApi("mHDiziH7d7ZRcVbkq3tF6yrPxgYDzjgfIsqHiSyOQoy0ZR5iWuDaQaz149vm");
var GetCountry = require('../country/GetCountry');

var querys = require('../../model/querys/querys');


function GetLeagues(leagueId) {

    this.leagueId = leagueId;

    this.result = {
        leagueInfo:{}
    };

    var self = this;

    this.selectLeagues = function () {
        return new Promise(function (resolve, reject) {
            leaguesQuery.checkDefineLeague(
                {sportmonks_id:self.leagueId},
                ['id','sportmonks_id','name','logo_path','s_country_id']
            )
                .then(function (data) {
                    if(data.length === 0){
                        self.getLeagueFromApi(self.leagueId)
                            .then(function (data) {
                                resolve(data)
                            })
                            .catch(function (error) {
                                resolve({error:'4534534534534',errMsg:error.toString()})
                            })
                    }else{
                        let oGetCountry = new GetCountry(data[0].s_country_id);

                        oGetCountry.selectCountry()
                            .then(function (selectCountryData) {
                                self.result.countryInfo = selectCountryData.countryInfo;
                                self.result.leagueInfo = data[0];
                                resolve(self.result)
                            })
                            .catch(function (error) {
                                resolve({error:'7532453435434',errMsg:error.toString()})
                            })
                    }
                })
                .catch(function (error) {
                    resolve({error:'8926598',errMsg:error.toString()})
                })
        })
    };


    this.getLeagueFromApi = function (id) {
        return new Promise(function (resolve, reject) {
            sportmonks.get(`v2.0/leagues/${id}`)
                .then(function (data) {
                    var insertData = {
                        sportmonks_id :data.data.id,
                        name:data.data.name,
                        logo_path:data.data.logo_path,
                        s_country_id:data.data.country_id
                    }
                    self.insertLeague(insertData)
                        .then(function (xxx) {
                            let oGetCountry = new GetCountry(data.data.country_id);
                            oGetCountry.selectCountry()
                                .then(function (selectCountryData) {
                                    self.result.countryInfo = selectCountryData;
                                    self.result.leagueInfo = data;
                                    resolve(self.result)
                                })
                                .catch(function (error) {
                                    resolve({error:'4835438345',errMsg:error.toString()})
                                })
                        })
                        .catch(function (error) {
                            resolve({error:'453543434534654',errMsg:error.toString()})
                        })
                })
                .catch(function (error) {
                    resolve({error:'325657454355756',errMsg:error.toString()})
                })
        });
    };

    this.insertLeague = function (insertData) {
        return new Promise(function (resolve, reject) {
            querys.insert2vPromise("s_leagues",insertData)
                .then(function (result) {
                    resolve(result)
                })
                .catch(function (error) {
                    resolve({error:true,errMsg:error.toString()})
                })
        })
    }


    // function callCountryObject() {
    //     return new Promise(function (resolve, reject) {
    //
    //     });
    // }
}


module.exports = GetLeagues;
