const countries = {
  data: [
      {
        id: 2,
        name: 'Poland',
        image_path: 'https://cdn.sportmonks.com/images/countries/png/short/pl.png',
        extra: [Object]
      }
  ]
};

const leagues = {
  "data": [
    {
      "id": 1,
      "league_id": 2,
      "season_id": 4,
      "stage_id": 1,
      "round_id": 14,
      "group_id": 9,
      "aggregate_id": 8,
      "venue_id": 29,
      "referee_id": 18,
      "localteam_id": 39,
      "visitorteam_id": 38,
      "weather_report": null,
      "commentaries": false,
      "attendance": null,
      "pitch": null,
      "winning_odds_calculated": false,
      "formations": {
        "localteam_formation": null,
        "visitorteam_formation": null
      },
      "scores": {
        "localteam_score": 0,
        "visitorteam_score": 0,
        "localteam_pen_score": 0,
        "visitorteam_pen_score": 0,
        "ht_score": null,
        "ft_score": null,
        "et_score": null
      },
      "time": {
        "status": "NS",
        "starting_at": {
          "date_time": "2018-11-23 18:00:00",
          "date": "2018-11-23",
          "time": "18:00:00",
          "timestamp": "1542996000",
          "timezone": "UTC"
        },
        "minute": null,
        "second": null,
        "added_time": null,
        "extra_minute": null,
        "injury_time": null
      },
      "coaches": {
        "localteam_coach_id": null,
        "visitorteam_coach_id": null
      },
      "standings": {
        "localteam_position": 13,
        "visitorteam_position": 7
      },
      "leg": "1/1",
      "colors": null,
      "deleted": false
    }
  ]
}

module.exports.LEAGUES = leagues;
module.exports.COUNTRIES = countries;
