var express = require('express');
var router = express.Router();
var sha1 = require('sha1');
var log = require("../../logs/log");
var querys = require("../../model/querys/querys");

const likesLeaguesPost =
[
    {sportmonks_id : 2, type : '0'},
    {sportmonks_id : 5, type : '0'},
    {sportmonks_id : 8, type : '0'},
    {sportmonks_id : 82, type : '0'},
    {sportmonks_id : 384, type : '0'},
    {sportmonks_id : 564, type : '0'}
];

/* GET home page. */
router.post('/sign-up',validateBody(), function(req, res, next) {
    if(!req.signUpError) {
        if (typeof req.body.email != "undefined" ||  req.body.email.length < 200) {
            querys.findByEmail(req.body.email)
                .then(function (userFind) {
                    if(userFind.length === 0){
                        var signUpParam = {
                            email: req.body.email,
                            password: sha1(req.body.password),
                            token: sha1(sha1(req.body.email) + "" + new Date().getTime()),
                        };
                        querys.insert2vPromise('users_db',signUpParam)
                            .then(function (data) {
                                var likesPost = [];
                                for(var i = 0; i < likesLeaguesPost.length; i++){
                                    likesPost.push([likesLeaguesPost[i].sportmonks_id,likesLeaguesPost[i].type,data.insertId])
                                }
                                var filds = ['sportmonks_id','type','user_id'];
                                querys.insertLoopPromise('users_likes',filds,likesPost)
                                    .then(function (datas) {
                                        res.json({error: false, msg: "1000",user:{token:signUpParam.token}});
                                        res.end();
                                    })
                                    .catch(function (error) {
                                        var errorData = {
                                            msgtext:"user likes not inserted",
                                            promiseError:error.toString(),
                                            date:new Date()
                                        }
                                        log.insertLog(JSON.stringify(errorData),"./logs/signup.txt")
                                        res.json({error: true, msg: "448", data: []});
                                        res.end();
                                    })

                            })
                            .catch(function (error) {
                                var errorData = {
                                    msgtext:"user not inserted",
                                    promiseError:error.toString(),
                                    date:new Date()
                                }
                                log.insertLog(JSON.stringify(errorData),"./logs/signup.txt")
                                res.json({error: true, msg: "447", data: []});
                                res.end();
                            })
                    }else{
                        var errorData = {
                            msgtext:"user Exists",
                            reqBody:req.body,
                            date:new Date()
                        }
                        log.insertLog(JSON.stringify(errorData),"./logs/signup.txt")
                        res.json({error: true, msg: "446", data: []});
                        res.end();
                    }
                })
                .catch(function (error) {
                    var errorData = {
                        promiseError:error.toString(),
                        msgtext:"Find user by this email",
                        errorText:req.signUpErrorText,
                        date:new Date()
                    }
                    log.insertLog(JSON.stringify(errorData),"./logs/signup.txt")
                    res.end();
                })
        }else {
            res.json({error: true, msg: "445", data: []});
            res.end();
        }
    } else{
        var errorData = {
            msgtext:"error params",
            reqBody:req.body,
            errorText:req.signUpErrorText,
            date:new Date()
        }
        log.insertLog(JSON.stringify(errorData),"./logs/signup.txt")
        res.json({error: true, msg: "444"});
        res.end();
    }
});

router.post('/login',validateBody(), function(req, res, next) {
    if(!req.signUpError) {
        var params = {
            email : req.body.email,
            password : sha1(req.body.password)
        }
        querys.findByMultyNamePromise('users_db', params, ['email','token'])
            .then(function (userFind) {
                //console.log('userFind',userFind)
                if(userFind.length === 0){
                    var errorData = {
                        msgtext:"no user",
                        date:new Date()
                    }
                    log.insertLog(JSON.stringify(errorData),"./logs/signup.txt")
                    res.json({error: true, msg: "449", data: []});
                    res.end();
                }else{
                    res.json({error: false, msg: "1000",user:userFind[0]});
                    res.end();
                }
            })
            .catch(function (error) {
                var errorData = {
                    promiseError:error.toString(),
                    msgtext:"Find user by this email",
                    errorText:req.signUpErrorText,
                    date:new Date()
                }
                log.insertLog(JSON.stringify(errorData),"./logs/signup.txt")
                res.end();
            })
    } else{
        var errorData = {
            msgtext:"error params",
            reqBody:req.body,
            errorText:req.signUpErrorText,
            date:new Date()
        }
        log.insertLog(JSON.stringify(errorData),"./logs/signup.txt")
        res.json({error: true, msg: "444"});
        res.end();
    }
});

router.post('/check-user', function(req, res, next) {
    //console.log('check user')
    if(typeof req.body.token != "undefined" && req.body.token.length > 0 ) {
        querys.findByToken(req.body.token)
            .then(function (user) {
                if (user.length === 1) {
                    // console.log('user[0]',user[0])
                    Promise.all([querys.findUserLikes(user[0].id,'0'),querys.findUserLikes(user[0].id,'1')])
                        .then(function ([likeLigas,likeTeams]) {
                            res.json({error: false, msg: "1000", user: user[0],likes:{leagues : likeLigas,teams : likeTeams}});
                            res.end();
                        })
                        .catch(function (error) {
                            var errorData = {
                                msgtext: "no user Likes",
                                reqBody: req.body,
                                date: new Date()
                            }
                            log.insertLog(JSON.stringify(errorData), "./logs/signup.txt")
                            res.json({error: true, msg: "789", data: []});
                            res.end();
                        })

                } else {
                    var errorData = {
                        msgtext: "no user",
                        reqBody: req.body,
                        date: new Date()
                    }
                    log.insertLog(JSON.stringify(errorData), "./logs/signup.txt")
                    res.json({error: true, msg: "123", data: []});
                    res.end();
                }
            })
            .catch(function (error) {
                var errorData = {
                    promiseError: error.toString(),
                    msgtext: "Find user by this email",
                    errorText: req.signUpErrorText,
                    date: new Date()
                }
                log.insertLog(JSON.stringify(errorData), "./logs/signup.txt");
                res.json({error: true, msg: "448", data: []});
                res.end();
            })
    }else{
        res.json({error: true, msg: "449", data: []});
        log.insertLog(JSON.stringify('error token define'), "./logs/signup.txt");
        res.end();
    }
});
module.exports = router;




function validateBody() {
    return function(req, res, next) {
        req.signUpError = false;
        req.signUpErrorText = [];
        let reqQuery = {
            email : req.body.email,
            password : req.body.password
        }
        let checkEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (typeof reqQuery.email == "undefined" || !checkEmail.test(reqQuery.email.toLowerCase())) {
            req.signUpError = true;
            req.signUpErrorText.push("email");
        }


        if(typeof reqQuery.password == "undefined" || reqQuery.password.length < 6) {
            req.signUpError = true;
            req.signUpErrorText.push("password");
        }else if(reqQuery.password.length == 0){
            req.signUpError = true;
            req.signUpErrorText.push("password");
        }

        next();
    }
}
