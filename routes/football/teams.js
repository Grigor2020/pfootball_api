var express = require('express');
var router = express.Router();
var querys = require('../../model/querys/querys');
var soprtmonksQuerys = require('../../model/sportmonks/football/soprtmonksQuerys');
var leagues = require('../../model/querys/leagues');
var log = require("../../logs/log");
var GetTeams = require('../../object/teams/GetTeams')
var topLeagues = require('../../topLeagues');

router.post('/topLeagues', function(req, res, next) {
    res.json({error:false,data:topLeagues.topLeagues})
});


router.post('/def-info', function(req, res, next) {
    if(typeof req.body.teamId !== "undefined"){
        soprtmonksQuerys.getFootballTeamById(req.body.teamId)
            .then(function (data) {
                res.json({error:false,data:data});
                res.end()
            })
            .catch(function (errorData) {
                log.insertLog(JSON.stringify(errorData),"./logs/all-logs.txt");
                res.json({error:true,data:[]});
                res.end()
            })
    }else{
        log.insertLog("req.body.teamId is Undefined","./logs/all-logs.txt");
        res.json({error:true,data:[]});
        res.end()
    }
});


router.post('/item', function(req, res, next) {
    var timeZone = req.body.timezone;
    var tz = "&tz="+timeZone;
    if(typeof req.body.teamId !== "undefined"){
        soprtmonksQuerys.getFootballTeamByIdWithOptions(req.body.teamId,tz)
            .then(function (data) {
                res.json({error:false,data:data});
                res.end()
            })
            .catch(function (errorData) {
                log.insertLog(JSON.stringify(errorData),"./logs/all-logs.txt");
                res.json({error:true,data:[]});
                res.end()
            })
    }else{
        log.insertLog("req.body.teamId is Undefined","./logs/all-logs.txt");
        res.json({error:true,data:[]});
        res.end()
    }
});

router.post('/fixtures', function(req, res, next) {
    if(typeof req.body.teamId !== "undefined"){
        soprtmonksQuerys.getFootballTeamFixtures(req.body.teamId)
            .then(function (data) {
                res.json({error:false,data:data});
                res.end()
            })
            .catch(function (errorData) {
                log.insertLog(JSON.stringify(errorData),"./logs/all-logs.txt");
                res.json({error:true,data:[]});
                res.end()
            })
    }else{
        log.insertLog("req.body.teamId is Undefined","./logs/all-logs.txt");
        res.json({error:true,data:[]});
        res.end()
    }
});


module.exports = router;
