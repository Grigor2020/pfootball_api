var express = require('express');
var router = express.Router();
var querys = require('../../model/querys/querys');
var country = require('../../model/querys/country');
var leagues = require('../../model/querys/leagues');
var log = require("../../logs/log");
var soprtmonksQuerys = require('../../model/sportmonks/football/soprtmonksQuerys');


router.post('/all', function(req, res, next) {
    // leagues.findFirstPageLeagues()
    country.findAllCountriesAndTheirLiguaes()
        .then(function (data) {
            // console.log('data',data)
            for(var i = 0; i < data.length; i++){
                var names = [];

                var dataNames = data[i].leagues_name.split('||');
                var dataIds = data[i].leagues_sid.split('||');
                for(var j = 0; j < dataNames.length; j++){
                    var obj = {
                        name : dataNames[j],
                        id : dataIds[j]
                    }
                    names.push(obj)
                }
                data[i].leagues = names;
                delete data[i].leagues_name;
                delete data[i].leagues_sid;
            }
            res.json({error:false,data:data})
        })
        .catch(function (error) {
            log.insertLog(JSON.stringify(error),"./logs/query2v.txt");
            res.json({error:true,errMsg : '51684635156'})
        })
});

router.post('/item', function(req, res, next) {
    //console.log('req.body.country_idv',req.body.country_id)
    if(typeof req.body.country_id !== "undefined"){
        var countryId = req.body.country_id;
        var tz = "&tz="+req.timezone;
        soprtmonksQuerys.getFootballCountry(countryId,tz)
            // soprtmonksQuerys.getFootballResultsBySeasonId(seasonId)
            .then(function (data) {
                // ste karanq grenq ekac team-info-i insert kam update
                // var oGetTeams = new GetTeams();
                // var list = data.data;
                // for(var i = 0; i < list.length; i++){
                //
                // }



                // console.log('data',list);
                res.json({error:false,data:data});
                res.end()
            })
            .catch(function (errorData) {
                log.insertLog(JSON.stringify(errorData),"./logs/all-logs.txt");
                res.json({error:true,data:[]});
                res.end()
            })
    }else{
        log.insertLog("req.body.seasonId is Undefined","./logs/all-logs.txt");
        res.json({error:true,data:[]});
        res.end()
    }
});


module.exports = router;
