var express = require('express');
var router = express.Router();
var querys = require('../../model/querys/querys');
var soprtmonksQuerys = require('../../model/sportmonks/football/soprtmonksQuerys');
var leagues = require('../../model/querys/leagues');
var log = require("../../logs/log");
var GetTeams = require('../../object/teams/GetTeams')
var topLeagues = require('../../topLeagues');

router.post('/topLeagues', function(req, res, next) {
    res.json({error:false,data:topLeagues.topLeagues})
});


router.post('/stand-for-popup', function(req, res, next) {
    if(typeof req.body.league_id !== "undefined"){
        soprtmonksQuerys.getFootballLeagueByLeagueId(req.body.league_id)
            .then(function (data) {
                var seasonId = data.season.data.id;
                soprtmonksQuerys.getFootballStandingsBySeasonIdNative(seasonId)
                    .then(function (result) {
                        res.json({error:false,data:result});
                        res.end()
                    })
                    .catch(function (error) {
                        log.insertLog(JSON.stringify(error),"./logs/all-logs.txt");
                        res.json({error:true,data:[]});
                        res.end()
                    })
            })
            .catch(function (errorData) {
                log.insertLog(JSON.stringify(errorData),"./logs/all-logs.txt");
                res.json({error:true,data:[]});
                res.end()
            })
    }else{
        log.insertLog("req.body.league_id is Undefined, URL = /stand-for-popup : ","./logs/all-logs.txt");
        res.json({error:true,data:[]});
        res.end()
    }
});

router.post('/topscorers', function(req, res, next) {
    if(typeof req.body.league_id !== "undefined"){
        soprtmonksQuerys.getFootballLeagueByLeagueId(req.body.league_id)
            .then(function (data) {
                var seasonId = data.season.data.id;
                soprtmonksQuerys.getFootballTopscorersBySeasonId(seasonId)
                    .then(function (result) {
                        res.json({error:false,data:result});
                        res.end()
                    })
                    .catch(function (error) {
                        log.insertLog(JSON.stringify(error),"./logs/all-logs.txt");
                        res.json({error:true,data:[]});
                        res.end()
                    })
            })
            .catch(function (errorData) {
                log.insertLog(JSON.stringify(errorData),"./logs/all-logs.txt");
                res.json({error:true,data:[]});
                res.end()
            })
    }else{
        log.insertLog("req.body.league_id is Undefined, URL = /stand-for-popup : ","./logs/all-logs.txt");
        res.json({error:true,data:[]});
        res.end()
    }
});
router.post('/def-info', function(req, res, next) {
    if(typeof req.body.league_id !== "undefined"){
        Promise.all([soprtmonksQuerys.getFootballLeagueByLeagueId(req.body.league_id)])
            .then(function ([data]) {
                res.json({error:false,data:data});
                res.end()
            })
            .catch(function (errorData) {
                log.insertLog(JSON.stringify(errorData),"./logs/all-logs.txt");
                res.json({error:true,data:[]});
                res.end()
            })
    }else{
        log.insertLog("req.body.seasonId is Undefined, URL = /def-info","./logs/all-logs.txt");
        res.json({error:true,data:[]});
        res.end()
    }
});


router.post('/results', function(req, res, next) {
    //console.log('req.body.seasonId',req.body.seasonId)
    //console.log('req.body.leagueId',req.body.leagueId)
    var tz = "&tz="+req.timezone;
    if(typeof req.body.seasonId !== "undefined" &&typeof req.body.leagueId !== "undefined" ){
        var seasonId = req.body.seasonId;
        var leagueId = req.body.leagueId;
        soprtmonksQuerys.getFootballRoundBySeasonID(leagueId,seasonId,tz)
            // soprtmonksQuerys.getFootballResultsBySeasonId(seasonId)
            .then(function (data) {
                // ste karanq grenq ekac team-info-i insert kam update
                // var oGetTeams = new GetTeams();
                // var list = data.data;
                // for(var i = 0; i < list.length; i++){
                //
                // }



                // console.log('data',list);
                res.json({error:false,data:data});
                res.end()
            })
            .catch(function (errorData) {
                log.insertLog(JSON.stringify(errorData),"./logs/all-logs.txt");
                res.json({error:true,data:[]});
                res.end()
            })
    }else{
        log.insertLog("req.body.seasonId is Undefined","./logs/all-logs.txt");
        res.json({error:true,data:[]});
        res.end()
    }
});

router.post('/standings', function(req, res, next) {
    var seasonId = req.body.seasonId;
    if(typeof req.body.seasonId !== "undefined"){
        soprtmonksQuerys.getFootballStandingsBySeasonId(seasonId)
            .then(function (data) {
                // ste karanq grenq ekac team-info-i insert kam update
                // var oGetTeams = new GetTeams();
                var list = data.data;
                res.json({error:false,data:list});
                res.end()
            })
            .catch(function (errorData) {
                log.insertLog(JSON.stringify(errorData),"./logs/all-logs.txt");
                res.json({error:true,data:[]});
                res.end()
            })
    }else{
        log.insertLog("req.body.seasonId is Undefined","./logs/all-logs.txt");
        res.json({error:true,data:[]});
        res.end()
    }
});

router.post('/del-like', function(req, res, next) {
    var leagueId = req.body.leagueId;
    if(typeof req.body.leagueId !== "undefined"){
        //console.log('leagueId',leagueId)
        querys.deletePromise('users_likes',{sportmonks_id:leagueId,type:'0'})
            .then(function () {
                res.json({error:false});
                res.end()
            })
            .catch(function (error) {
                res.json({error:true});
                res.end()
            })
    }else{
        log.insertLog("req.body.seasonId is Undefined","./logs/all-logs.txt");
        res.json({error:true,data:[]});
        res.end()
    }
});
router.post('/add-like', function(req, res, next) {
    var leagueId = req.body.leagueId;
    var token = req.body.token;
    var type = req.body.type;
    // **********
    if(typeof req.body.leagueId !== "undefined"){
        querys.findByToken(req.body.token)
            .then(function () {

            })
            .catch(function (error) {
                log.insertLog("no user find","./logs/all-logs.txt");
                res.json({error:true,data:[]});
                res.end()
            })
        //console.log('leagueId',leagueId)
        querys.deletePromise('users_likes',{sportmonks_id:leagueId,type:'0'})
            // .then(function () {
            //     res.json({error:false});
            //     res.end()
            // })
            // .catch(function (error) {
            //     res.json({error:true});
            //     res.end()
            // })
    }else{
        log.insertLog("req.body.seasonId is Undefined","./logs/all-logs.txt");
        res.json({error:true,data:[]});
        res.end()
    }
});



module.exports = router;
