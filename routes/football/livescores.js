var express = require('express');
var router = express.Router();
var SportmonksApi = require('sportmonks').SportmonksApi;
var sportmonks = new SportmonksApi("mHDiziH7d7ZRcVbkq3tF6yrPxgYDzjgfIsqHiSyOQoy0ZR5iWuDaQaz149vm");
var GetLeagues = require('../../object/leagues/GetLeagues');
var GetTeams = require('../../object/teams/GetTeams');
var log = require("../../logs/log");
var topLeagues = require('../../topLeagues');
var request = require('request');
var statics = require('../../static');


// router.get('/asdasdasd', function(req, res, next) {

    // var includes = "localTeam, visitorTeam,league,season,round,inplay,events,stats";
    // var includes = "localTeam, visitorTeam, goals, cards, other, corners, lineup, stats, league,  events";
    // const options = {
    //     url: statics.SPORTMONKS_API_URL+"/livescores?api_token="+ statics.SPORTMONKS_API_TOKEN+"&include="+includes+"&page=2",
    //     method : "GET"
    // };
    // request(options, function (error, response, body) {
    //     if (!error && response.statusCode == 200) {
    //         const result = JSON.parse(body);
    //         console.log('result',result)
    //         console.log('bbbbbbbb',result.data.length)
    //         if(!result.error){
    //             let results = setPriorityAPI(result.data);
    //             res.json({error:false,data : results})
    //         }
    //     }
    // });
    // sportmonks.get(`v2.0/leagues/564`, ).then( function(data) {
    // sportmonks.get(`v2.0/fixtures/11886283?include=cards,goals`).then( function(data) {
    // sportmonks.get(`v2.0/stages/season/{id}`, {id:16326}).then( function(data) {
    // sportmonks.get(`v2.0/livescores`,{leagues :'564'}).then( function(data) {
    // sportmonks.get(`v2.0/rounds/175896`).then( function(data) {

    //pagination info can be found in
    // console.log('assadasddsa',data.data.length)


    // res.json({data:data})
    // });

// });
router.post('/for_spec_day', function(req, res, next) {
    // console.log('req.body.day',req.body.day)
    // console.log('req.body.page',req.body.page)
    // console.log('req.body.timezone',req.body.timezone)
    // var day = '';
    // var month = '';
    // var year = '';
    // for(var i = 0; i < req.body.day.length; i++){
    //     if(i == 0 || i == 1){
    //         day+= req.body.day[i]
    //     }
    //     if(i == 3 || i == 4){
    //         month+= req.body.day[i]
    //     }
    //     if(i == 6 || i == 7 || i == 8 || i == 9){
    //         year+= req.body.day[i]
    //     }
    // }
    var page = typeof req.body.page === "undefined" ? 1 : req.body.page;
    // var todayDate = year+'-'+month+'-'+day
    // console.log('todayDate',todayDate)
    var timeZone = req.body.timezone;
    var tz = "&tz="+timeZone;
    var includes = "localTeam, visitorTeam, goals, league  ";
    const options = {
        url: statics.SPORTMONKS_API_URL+"/fixtures/date/"+req.body.day+"/?api_token="+ statics.SPORTMONKS_API_TOKEN+"&include="+includes+"&page="+page+tz,
        method : "GET"
    };
    //console.log('options',options)
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                let results = setPriorityAPI(result.data);
                res.json({error:false,data : results})
            }
        }
    });
});

router.post('/now', function(req, res, next) {

    var timeZone = req.body.timezone;
    var tz = "&tz="+timeZone;
    var includes = "localTeam, visitorTeam, goals, league  ";
    const options = {
        url: statics.SPORTMONKS_API_URL+"/livescores/now?api_token="+ statics.SPORTMONKS_API_TOKEN+"&include="+includes+tz,
        method : "GET"
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                let results = setPriorityLiveAPI(result.data);
                res.json({error:false,data : results})
            }
        }
    });
});

router.post('/', function(req, res, next) {

    // ***************************FIRST CALL************************************************
    // var allRequests = [
    //   sportmonks.get('v2.0/livescores',{page:typeof req.body.page === "undefined" ? 1 : req.body.page}),
    // ];
    // Promise.all(allRequests)
    //     .then( function([result]) {
    //         var resultAll = [];
    //         // checkPages(result.data,0,resultAll,function (pagesData) {
    //         //
    //         // })
    //         var resultData = [];
    //         checkData(result.data,0,resultData, function (data) {
    //             let results = setPriority(data);
    //
    //             console.log('results',typeof results)
    //             data.topLeagues = topLeagues;
    //         console.log('result',result)
    //             res.json({
    //                 error:false,
    //                 msg:'1000',
    //                 data:results,
    //                 topLeagues : topLeagues.topLeagues
    //             })
    //         });
    //     })
    //     .catch(function (error) {
    //       console.log('error', error);
    //       res.json({error:true,errMsg : error.toString()})
    //     })

    //********************************END FIRST CALL ****************************
    var tz = "&tz="+req.timezone;

    var page = typeof req.body.page === "undefined" ? 1 : req.body.page;
    var includes = "localTeam, visitorTeam, goals, league";
    const options = {
        url: statics.SPORTMONKS_API_URL+"/livescores?api_token="+ statics.SPORTMONKS_API_TOKEN+"&include="+includes+"&page="+page+tz,
        method : "GET"
    };

    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                let results = setPriorityAPI(result.data);
                res.json({error:false,data : results})
            }
        }
    });
});

module.exports = router;

// function checkPages(data,num,resultData,cb) {
//
// }

function checkData(data,num,resultData,cb) {
    var localResult = {};
    if(num < data.length){
        let oGetLeagues = new GetLeagues(data[num]['league_id']);
        let tGetTeams = new GetTeams(data[num]['localteam_id']);
        let vGetTeams = new GetTeams(data[num]['visitorteam_id']);

        Promise.all([
            oGetLeagues.selectLeagues(),
            tGetTeams.selectTeam(),
            vGetTeams.selectTeam()
        ])
            .then(function ([leaguesResult,localTeamResult,visitorTeamResult]) {
                localResult.mainId = data[num]['id'];
                localResult.mainData = data[num];
                localResult.leaguesResult = leaguesResult;
                localResult.localTeamResult = localTeamResult;
                localResult.visitorTeamResult = visitorTeamResult;
                num++;
                resultData.push(localResult);
                checkData(data,num,resultData,cb)
            })
            .catch(function (error) {
                var errorData = {
                    code:error.code,
                    errno:error.errno,
                    devData:{
                        queryFunction:"PromiseAll get all first page info",
                        date:new Date()
                    }
                }
                log.insertLog(JSON.stringify(errorData),"./logs/callApiData.txt");
                num++;
                checkData(data,num,resultData,cb)
            })

    }else{
        cb(resultData)
    }
}

function setPriority(data) {
    let allInfo = [];


    let checkArray = []
    let sectionContent = [];
    for(let i = 0; i < data.length; i++){
        if(checkArray.indexOf(data[i].mainData.league_id)  === -1){
            checkArray.push(data[i].mainData.league_id);
        }
    }
    for(var i = 0; i < checkArray.length; i++){
        var oneArr = [];
        for(var j = 0; j < data.length; j++){
            if(data[j].mainData.league_id == checkArray[i]){
                oneArr.push(data[j]);
            }
        }
        allInfo.push(oneArr);
    }

    return allInfo;
}
function setPriorityAPI(data) {
    let allInfo = [];


    let checkArray = []
    let sectionContent = [];
    for(let i = 0; i < data.length; i++){
        if(checkArray.indexOf(data[i].league_id)  === -1){
            checkArray.push(data[i].league_id);
        }
    }
    for(var i = 0; i < checkArray.length; i++){
        var oneArr = [];
        for(var j = 0; j < data.length; j++){
            if(data[j].league_id == checkArray[i] ){
                oneArr.push(data[j]);
            }
        }
        allInfo.push(oneArr);
    }

    return allInfo;
}

function setPriorityLiveAPI(data) {
    let allInfo = [];
    //
    // console.log('.................data',data[0]['time']['status'])
    var liveData = [];
    for(let k = 0; k < data.length; k++){
        if(data[k].time.status === 'LIVE' || data[k].time.status === 'HT'){
            liveData.push(data[k]);
        }
    }

    //console.log('liveData',liveData)
    let checkArray = []
    let sectionContent = [];
    for(let i = 0; i < liveData.length; i++){
        if(checkArray.indexOf(liveData[i].league_id)  === -1){
            checkArray.push(liveData[i].league_id);
        }
    }
    for(var i = 0; i < checkArray.length; i++){
        var oneArr = [];
        for(var j = 0; j < liveData.length; j++){
            if(liveData[j].league_id == checkArray[i]){
                oneArr.push(liveData[j]);
            }
        }
        allInfo.push(oneArr);
    }

    return allInfo;
}
function sortTransferDates(data) {
    data.sort(compare);
    return data;
}
function compare(a, b) {
    // Use toUpperCase() to ignore character casing
    const A = parseInt(a);
    const B = parseInt(b);

    let comparison = 0;
    if (A > B) {
        comparison = 1;
    } else if (A < B) {
        comparison = -1;
    }
    return comparison;
}
