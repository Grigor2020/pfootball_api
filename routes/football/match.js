var express = require('express');
var router = express.Router();
var querys = require('../../model/querys/querys');
var soprtmonksQuerys = require('../../model/sportmonks/football/soprtmonksQuerys');
var leagues = require('../../model/querys/leagues');
var log = require("../../logs/log");
var GetTeams = require('../../object/teams/GetTeams')
var topLeagues = require('../../topLeagues');


router.post('/get-h2h', function(req, res, next) {
    if(typeof req.body.team1id !== "undefined" && typeof req.body.team2id !== "undefined"){
        var team1id = req.body.team1id;
        var team2id = req.body.team2id;
        var timezone = req.body.timezone || req.timezone
        soprtmonksQuerys.getMatchH2H(team1id,team2id,timezone)
            .then(function (data) {
                console.log('////////////', data)
                res.json({error:false,data:data})
            })
            .catch(function (error) {
                console.log('//////////// ERROR', error)
                log.insertLog(JSON.stringify(error),"./logs/all-logs.txt");
                res.json({error:true,data:[],msg:'2134831565'})
            })
    }else{
        res.json({error:true,data:[],msg:'84294846532513'})
    }
});


router.post('/get-match', function(req, res, next) {

    if(typeof req.body.matchId !== "undefined"){
        soprtmonksQuerys.getMatchDetailsByMatchId(req.body.matchId,req.plusSeconds)
            .then(function (data) {

                res.json({error:false,data:data})
            })
            .catch(function (error) {
                log.insertLog(JSON.stringify(error),"./logs/all-logs.txt");
                res.json({error:true,data:[],msg:'2134831565'})
            })
    }else{
        res.json({error:true,data:[],msg:'8429435165'})
    }
});


module.exports = router;
